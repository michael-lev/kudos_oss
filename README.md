# Build a Simple CRUD App with Python, Flask, and React

This tutorial show how to build a basic CRUD (Create, Read, Update, and Delete) application using Python with Flask as the API and React for the front-end.

Please read the [Build a Simple CRUD App with Python, Flask, and React](https://developer.okta.com/blog/2018/12/20/crud-app-with-python-flask-react) to see the step-by-step instructions for creating this application.

## MUST STEP:

You *MUST* export the client_id and issuer envs properly.
for example:
```
export CLIENT_ID="0oa291*****Gf357"

export ISSUER="https://dev-***.okta.com/oauth2/default"
```
(Also possible to add to .env.local -> it's on .gitignore)


#### Prepare your project

- `cd /app/http/web/app`
- `npm install`
- `npm run-script build`


## Help
Please post any questions as comments on the [blog post](https://developer.okta.com/blog/2018/12/20/crud-app-with-python-flask-react), or visit our [Okta Developer Forums](https://devforum.okta.com/). You can also email developers@okta.com if you'd like to create a support ticket.

## License
Apache 2.0, see [LICENSE](LICENSE).
